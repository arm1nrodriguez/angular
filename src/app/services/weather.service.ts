import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  zipCodes = [];
  zipCodeStored = [];


  constructor(public httpClient: HttpClient) { }

  storeZipCodes(zipCode): void {
    this.zipCodes.push(zipCode);
    localStorage.setItem('zipCodes', JSON.stringify(this.zipCodes));
    this.zipCodeStored = JSON.parse(localStorage.getItem('zipCodes'));
  }

  getWeatherByZipCode(zipcode): Observable<any>{
    return this.httpClient.get(`https://api.openweathermap.org/data/2.5/weather?zip=${zipcode},us&appid=5a4b2d457ecbef9eb2a71e480b947604&units=metric`);
  }


  getByZipCode(zipCode): Observable<any>{
    return this.httpClient.get(`https://api.openweathermap.org/data/2.5/forecast/daily?zip=${zipCode},us&appid=5a4b2d457ecbef9eb2a71e480b947604&units=metric`);
  }

}
