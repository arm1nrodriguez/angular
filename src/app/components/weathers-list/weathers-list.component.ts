import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {WeatherService} from '../../services/weather.service';

@Component({
  selector: 'app-weathers-list',
  templateUrl: './weathers-list.component.html',
  styleUrls: ['./weathers-list.component.scss']
})
export class WeathersListComponent implements OnInit {
  @Input() data;
  @Input() zipCode;

  constructor(public router: Router, public weatherService: WeatherService) { }

  ngOnInit(): void {

    if (localStorage.getItem('zipCodes')){
      this.zipCode = JSON.parse(localStorage.getItem('zipCodes'));
    }
    if (localStorage.getItem('data')) {
      this.data = JSON.parse(localStorage.getItem('data'));
    }
  }

  onClose(i): void{
    this.data.splice(i, 1);
    this.zipCode.splice(i, 1);
    localStorage.setItem('data', JSON.stringify(this.data));
    localStorage.setItem('zipCodes', JSON.stringify(this.zipCode));
  }
}
