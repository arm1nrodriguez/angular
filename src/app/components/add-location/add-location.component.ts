import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {WeatherService} from '../../services/weather.service';
import {MatDialog} from '@angular/material/dialog';


@Component({
  selector: 'app-add-location',
  templateUrl: './add-location.component.html',
  styleUrls: ['./add-location.component.scss']
})
export class AddLocationComponent implements OnInit {
  disabledButton = false;
  data = [];
  zipCodes = [];

  constructor(public weatherService: WeatherService, public dialog: MatDialog) { }

  dataForm = new FormGroup({
    zipCode: new FormControl('', [Validators.required])
  });

  ngOnInit(): void {
    if (localStorage.getItem('zipCodes')){
      this.zipCodes = JSON.parse(localStorage.getItem('zipCodes'));
    }
    if (localStorage.getItem('data')) {
      this.data = JSON.parse(localStorage.getItem('data'));
    }

  }

  onSubmit(): void {
    if (localStorage.getItem('zipCodes')){
      this.zipCodes = JSON.parse(localStorage.getItem('zipCodes'));
    }
    if (localStorage.getItem('data')) {
      this.data = JSON.parse(localStorage.getItem('data'));
    }
    this.zipCodes.push(this.dataForm.value.zipCode);
    this.weatherService.getWeatherByZipCode(this.dataForm.value.zipCode).subscribe(response => {
    this.data.push(response);
    localStorage.setItem('data', JSON.stringify(this.data));
    localStorage.setItem('zipCodes', JSON.stringify(this.zipCodes));
    });
    this.cleanForm();
  }

  cleanForm(): void{
    this.dataForm.setValue({zipCode: ''});
  }


}
