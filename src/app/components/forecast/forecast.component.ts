import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {WeatherService} from '../../services/weather.service';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss']
})
export class ForecastComponent implements OnInit {
  zipCode = 0;
  data = [];

  constructor(public router: Router,
              public weatherService: WeatherService,
              private activatedRoute: ActivatedRoute ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params: Params) => {
      if (params.zipcode){
        this.zipCode = params.zipcode;
        this.weatherService.getByZipCode(this.zipCode).subscribe((response => {
          this.data = response;
        }));
      }
    });
  }

  getBack(): void {
    this.router.navigate(['']);
  }

}
