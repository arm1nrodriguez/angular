import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AddLocationComponent} from './components/add-location/add-location.component';
import {ForecastComponent} from './components/forecast/forecast.component';

const routes: Routes = [
  {path: '', component: AddLocationComponent},
  {path: 'forecast/:zipcode', component: ForecastComponent},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
